/**
* @author Sen Lin
* @file Tester.cpp
* This file contains the function definitions for the Tester class.
*/

#include "Tester.hpp"

void Tester::RunTests()
{
	Test_Init();

    Test_IsEmpty();
    Test_IsFull();
    Test_Size();
    Test_GetCountOf();
    Test_Contains();

    Test_PushFront();
    Test_PushBack();

    Test_Get();
    Test_GetFront();
    Test_GetBack();

    Test_PopFront();
    Test_PopBack();
    Test_Clear();

    Test_ShiftRight();
    Test_ShiftLeft();

    Test_RemoveAtIndex();
	Test_RemoveWithValue();
    Test_Insert();
}

void Tester::DrawLine()
{
    cout << endl;
    for ( int i = 0; i < 80; i++ )
    {
        cout << "-";
    }
    cout << endl;
}

void Tester::Test_Init()
{
    DrawLine();
    cout << "TEST: Test_Init" << endl;

	// Put tests here

	{ 	// Test_Init - Test 1
		cout << "\nTest 1:\nCreate a list, do nothing.\n"
			 <<  "Size() should return 0.\n" <<endl;
		List<string> testlist;
		int expectedSize = 0;
		int actualSize = testlist.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // Test end

	{ 	// Test_Init - Test 2
		cout << "\nTest 2:\nCreate a list, do nothing.\n"
			<< "IsEmpty() should return true.\n" << endl;
		List<string> testlist;
		bool expectedResult = true;
		bool actualResult = testlist.IsEmpty();

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;
			
		}
		else
		{
			cout << "Expected result: false" << endl;
		
		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // Test end

	{ 	// Test_Init - Test 3
		cout << "\nTest 3:\nCreate a list, do nothing.\n"
			<< "IsFull() should return false.\n" << endl;
		List<string> testlist;
		bool expectedResult = false;
		bool actualResult = testlist.IsFull();

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // Test end
}

void Tester::Test_ShiftRight()
{
    DrawLine();
    cout << "TEST: Test_ShiftRight" << endl;

	// Put tests here

	{ 	// Test_ShiftRight - Test 1
		cout << "\nTest 1:\nCreate a list, do nothing.\n"
			 << "Input 0.\n"
			<< "ShiftRight(0) returns true.\n" << endl;
		List<string> testlist;
		bool expectedResult = true;
		bool actualResult = testlist.ShiftRight(0);

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // Test end

	{ 	// Test_ShiftRight - Test 2
		cout << "\nTest 2:\nCreate a List, insert \"A\" at [0], \"B\" at [1], \"C\" at [2].\n"
			 << "Input 0.\n"
			 << "Get(0) returns \"A\", Get(2) returns \"B\", Get(2) returns \"C\"\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		bool expectedResult = true;
		bool actualResult = testlist.ShiftRight(1);
		string expectedValue_0 = "A";
		string* actualValue_0 = testlist.Get(0);
		string expectedValue_2 = "B";
		string* actualValue_2 = testlist.Get(2);
		string expectedValue_3 = "C";
		string* actualValue_3 = testlist.Get(3);

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}

		cout << "\nExpected value: " << expectedValue_0 << endl;
		cout << "Actual value:   " << *actualValue_0 << endl;

		if (*actualValue_0 == expectedValue_0)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}

		cout << "\nExpected value: " << expectedValue_2 << endl;
		cout << "Actual value:   " << *actualValue_2 << endl;

		if (*actualValue_2 == expectedValue_2)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}

		cout << "\nExpected value: " << expectedValue_3 << endl;
		cout << "Actual value:   " << *actualValue_3 << endl;

		if (*actualValue_3 == expectedValue_3)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}

	}   // Test end

	{ 	// Test_ShiftRight - Test 3

		cout << "\nTest 3:\nCreate a List, insert \"A\" at [0], \"B\" at [1], \"C\" at [2].\n"
			<< "Input 3.\n"
			<< "ShiftRight(3) returns true.\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		bool expectedResult = true;
		bool actualResult = testlist.ShiftRight(3);

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // Test end
}

void Tester::Test_ShiftLeft()
{
    DrawLine();
    cout << "TEST: Test_ShiftLeft" << endl;

	// Put tests here

	{ 	// Test_ShiftLeft - Test 1
		cout << "\nTest 1:\nCreate a list, do nothing.\n"
			<< "Input 0.\n"
			<< "ShiftLeft(0) returns true.\n" << endl;
		List<string> testlist;
		bool expectedResult = true;
		bool actualResult = testlist.ShiftLeft(0);

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End Test 1

	{ 	// Test_ShiftLeft - Test 2
		cout << "\nTest 2:\nCreate a List, insert \"A\" at [0], \"B\" at [1], \"C\" at [2].\n"
			<< "Input 0.\n"
			<< "Get(0) returns \"A\", Get(1) returns \"C\"\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		bool expectedResult = true;
		bool actualResult = testlist.ShiftLeft(1);
		string expectedValue_0 = "A";
		string* actualValue_0 = testlist.Get(0);
		string expectedValue_1 = "C";
		string* actualValue_1 = testlist.Get(1);

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}

		cout << "\nExpected value: " << expectedValue_0 << endl;
		cout << "Actual value:   " << *actualValue_0 << endl;

		if (*actualValue_0 == expectedValue_0)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}

		cout << "\nExpected value: " << expectedValue_1 << endl;
		cout << "Actual value:   " << *actualValue_1 << endl;

		if (*actualValue_1 == expectedValue_1)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}

	}   // End test 2

	{ 	// Test_ShiftLeft - Test 3
		cout << "\nTest 3:\nCreate a List, insert \"A\" at [0], \"B\" at [1], \"C\" at [2].\n"
			<< "Input 3.\n"
			<< "ShiftLeft(3) returns true. \n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		bool expectedResult = true;
		bool actualResult = testlist.ShiftLeft(3);

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 3
}

void Tester::Test_Size()
{
    DrawLine();
    cout << "TEST: Test_Size" << endl;

    {   // Test_Size - Test 1
		cout << "\nTest 1:\nCreate a list, do nothing.\n"
			<< "Size() returns 0.\n" << endl;

        List<string> testlist;
        int expectedSize = 0; 
        int actualSize = testlist.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
    }   // Test end

    {   // Test_Size - Test 2
		cout << "\nTest 2:\nCreate a list, insert 1 item.\n"
			<< "Size() returns 1.\n" << endl;
		List<string> testlist;
        testlist.PushBack( "A");

        int expectedSize = 1;
        int actualSize = testlist.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
    }   // Test end
}

void Tester::Test_IsEmpty()
{
    DrawLine();
    cout << "TEST: Test_IsEmpty" << endl;

	// Put tests here

	{   // Test_IsEmpty - Test 1
		cout << "\nTest 1:\nCreate a list, do nothing.\n"
			<< "IsEmpty() returns true.\n" << endl;
		List<string> testlist;
		bool expectedResult = true;
		bool actualResult = testlist.IsEmpty();

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	} // end Test 1

	{   // Test_IsEmpty - Test 2
		cout << "\nTest 2:\nCreate a list, insert \"A\"\n"
			<< "IsEmpty() returns false.\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		bool expectedResult = false;
		bool actualResult = testlist.IsEmpty();

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	} // end Test 2
}

void Tester::Test_IsFull()
{
    DrawLine();
    cout << "TEST: Test_IsFull" << endl;

    // Put tests here

	{   // Test_IsFull - Test 1
		cout << "\nTest 1:\nCreate a list, do nothing.\n"
			<< "IsFull() returns false.\n" << endl;
		List<string> testlist;
		bool expectedResult = false;
		bool actualResult = testlist.IsFull();

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	} // end Test 1

	{   // Test_IsFull - Test 2
		cout << "\nTest 2:\nCreate a list, insert 2 items.\n"
			<< "IsFull() returns false.\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		bool expectedResult = false;
		bool actualResult = testlist.IsFull();

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	} // end Test 2

	{   // Test_IsFull - Test 3
		cout << "\nTest 3:\nCreate a list, insert 100 items.\n"
			<< "IsFull() returns true.\n" << endl;
		List<string> testlist;

		for (int i = 0; i < 100; i++)
		{
			testlist.PushBack("A");
		}

		bool expectedResult = true;
		bool actualResult = testlist.IsFull();

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	} // end Test 3
}

void Tester::Test_PushFront()
{
    DrawLine();
    cout << "TEST: Test_PushFront" << endl;

    // Put tests here

	{ 	// Test_PushFront - Test 1
		cout << "\nTest 1:\nCreate a list, do nothing.\n"
			<< "Input \"A\"\n"
			<< "PushFront(\"A\") should return true. Size() returns 1.\n" << endl;
		List<string> testlist;
		bool expectedResult = true;
		bool actualResult = testlist.PushFront("A");
		int expectedSize = 1;
		int actualSize = testlist.Size();

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}

		cout << "\nExpected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End Test 1

	{ 	// Test_PushFront - Test 2
		cout << "\nTest 2:\nCreate a list, insert 100 items.\n"
			<< "Input \"A\""
			<< "PushFront(\"A\") should return false.\n" << endl;
		List<string> testlist;
		for (int i = 0; i < 100; i++)
		{
			testlist.PushFront("A");
		}
		bool expectedResult = false;
		bool actualResult = testlist.PushFront("A");

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End Test 2

	{ 	// Test_PushFront - Test 3
		cout << "\nTest 3:\nCreate a list, do nothing.\n"
			<< "Input \"A\", \"B\", \"C\"\n"
			<< "Get(0) returns \"C\", Get(1) returns \"B\", Get(2) returns \"A\"\n" << endl;
		List<string> testlist;
		testlist.PushFront("A");
		testlist.PushFront("B");
		testlist.PushFront("C");

		string expectedValue_0 = "C";
		string expectedValue_1 = "B";
		string expectedValue_2 = "A";

		string* actualValue_0 = testlist.Get(0);
		string* actualValue_1 = testlist.Get(1);
		string* actualValue_2 = testlist.Get(2);

		cout << "Expected value at 0: " << expectedValue_0 << endl;
		cout << "Expected value at 1: " << expectedValue_1 << endl;
		cout << "Expected value at 2: " << expectedValue_2 << endl;

		cout << "\nActual value at 0: " << *actualValue_0 << endl;
		cout << "Actual value at 1: " << *actualValue_1 << endl;
		cout << "Actual value at 2: " << *actualValue_2 << endl;

		if (expectedValue_0 == *actualValue_0 &&
			expectedValue_1 == *actualValue_1 &&
			expectedValue_2 == *actualValue_2)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}   // End Test 3
}

void Tester::Test_PushBack()
{
    DrawLine();
    cout << "TEST: Test_PushBack" << endl;

	// Put tests here

	{ 	// Test_PushBacl - Test 1
		cout << "\nTest 1:\nCreate a list, do nothing.\n"
			<< "Input \"A\"\n"
			<< "PushBack(\"A\") should return true. Size() returns 1.\n" << endl;
		List<string> testlist;
		bool expectedResult = true;
		bool actualResult = testlist.PushBack("A");
		int expectedSize = 1;
		int actualSize = testlist.Size();

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}

		cout << "\nExpected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End Test 1

	{ 	// Test_PushFront - Test 2
		cout << "\nTest 2:\nCreate a list, insert 100 items.\n"
			<< "Input \"A\"\n"
			<< "PushBack(\"A\") should return false.\n" << endl;
		List<string> testlist;
		for (int i = 0; i < 100; i++)
		{
			testlist.PushBack("A");
		}
		bool expectedResult = false;
		bool actualResult = testlist.PushFront("A");

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End Test 2

	{ 	// Test_PushFront - Test 3
		cout << "\nTest 3:\nCreate a list, do nothing.\n"
			<< "Input \"A\", \"B\", \"C\"\n"
			<< "Get(0) returns \"A\", Get(1) returns \"B\", Get(3) returns \"C\"\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");

		string expectedValue_0 = "A";
		string expectedValue_1 = "B";
		string expectedValue_2 = "C";

		string* actualValue_0 = testlist.Get(0);
		string* actualValue_1 = testlist.Get(1);
		string* actualValue_2 = testlist.Get(2);


		cout << "Expected value at 0: " << expectedValue_0 << endl;
		cout << "Expected value at 1: " << expectedValue_1 << endl;
		cout << "Expected value at 2: " << expectedValue_2 << endl;

		cout << "\nActual value at 0: " << *actualValue_0 << endl;
		cout << "Actual value at 1: " << *actualValue_1 << endl;
		cout << "Actual value at 2: " << *actualValue_2 << endl;

		if (expectedValue_0 != *actualValue_0 ||
			expectedValue_1 != *actualValue_1 ||
			expectedValue_2 != *actualValue_2)
		{
			cout << "Test Failed" << endl;
		}
		else
		{
			cout << "Test Passed" << endl;
		}
	}   // End Test 3
	
}

void Tester::Test_PopFront()
{
    DrawLine();
    cout << "TEST: Test_PopFront" << endl;

    // Put tests here

	{ 	// Test_PopFront - Test 1
		cout << "\nTest 1:\nCreate a list, do nothing.\n"
			<< "PopFront() should return false.\n" << endl;
		List<string> testlist;
		bool expectedResult = false;
		bool actualResult = testlist.PopFront();

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 1


	{ 	// Test_PopFront - Test 2
		cout << "\nTest 2:\nCreate a list, insert \"A\".\n"
			<< "PopFront() should return true.\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		bool expectedResult = true;
		bool actualResult = testlist.PopFront();

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 2


	{ 	// Test_PopFront - Test 3
		cout << "\nTest 3:\nCreate a List, insert  \"A\" at [0], \"B\" at [1].\n"
			<< "Get[0] does not equal \"A\".\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PopFront();

		if (*testlist.Get(0) == "A")
		{
			cout << "Test Failed" << endl;
		}
		else
		{
			cout << "Test Passed" << endl;
		}
	}   // End test 3
}

void Tester::Test_PopBack()
{
    DrawLine();
    cout << "TEST: Test_PopBack" << endl;

    // Put tests here

	{ 	// Test_PopBack - Test 1
		cout << "\nTest 1:\nCreate a list, do nothing.\n"
			<< "PopBack() should return false.\n" << endl;
		List<string> testlist;
		bool expectedResult = false;
		bool actualResult = testlist.PopBack();

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 1


	{ 	// Test_PopBack - Test 2
		cout << "\nTest 2:\nCreate a list, insert \"A\".\n"
			<< "PopBack() should return true.\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		bool expectedResult = true;
		bool actualResult = testlist.PopBack();

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 2


	{ 	// Test_PopBack - Test 3
		cout << "\nTest 3:\nCreate a List, insert  \"A\" at [0], \"B\" at [1].\n"
			<< "Size() returns 1\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PopBack();
		int expectedSize = 1;
		int actualSize = testlist.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 3
}


void Tester::Test_Clear()
{
	DrawLine();
	cout << "TEST: Test_Get" << endl;

	// Put tests here

	{ 	// Test_Clear - Test 1
		cout << "\nTest 1:\nCreate a list, do nothing.\n"
			<< "Size() should return 0.\n" << endl;
		List<string> testlist;
		int expectedSize = 0;
		int actualSize = testlist.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 1


	{ 	// Test_Clear - Test 2
		cout << "\nTest 1:\nCreate a list, insert 1 item.\n"
			<< "Size() should return 0.\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		testlist.Clear();
		int expectedSize = 0;
		int actualSize = testlist.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 2

	{ 	// Test_Clear - Test 3
		cout << "\nTest 1:\nCreate a list, insert 100 items.\n"
			<< "Size() should return 0.\n" << endl;
		List<string> testlist;
		for (int i = 0; i < 100; i++)
		{
			testlist.PushBack("A");
		}
		testlist.Clear();
		int expectedSize = 0;
		int actualSize = testlist.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 3

}

void Tester::Test_Get()
{
    DrawLine();
    cout << "TEST: Test_Get" << endl;

    // Put tests here

	{ 	// Test_Get - Test 1
		cout << "\nTest 1:\nCreate a list, do nothing.\n"
			<< "Input 0\n"
			<< "Get(0) should return nullptr.\n" << endl;
		List<string> testlist;
		string* actualValue = testlist.Get(0);

		if (actualValue == nullptr) // the expected value is nullptr
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 1

	{ 	// Test_Get - Test 2
		cout << "\nTest 2:\nCreate a list, insert \"A\".\n"
			<< "Input 0\n"
			<< "Get(0) should return \"A\".\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		string expectedValue = "A";
		string* actualValue = testlist.Get(0);

		if (*actualValue == expectedValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 2
}

void Tester::Test_GetFront()
{
    DrawLine();
    cout << "TEST: Test_GetFront" << endl;

    // Put tests here

	{ 	// Test_GetFront - Test 1
		cout << "\nTest 1:\nCreate a list, do nothing.\n"
			<< "GetFront() should return nullptr.\n" << endl;
		List<string> testlist;
		string* actualValue = testlist.GetFront();

		if (actualValue == nullptr) // the expected value is nullptr
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 1


	{ 	// Test_GetFront - Test 2
		cout << "\nTest 2:\nCreate a list, insert \"A\".\n"
			<< "GetFront() should return \"A\".\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		string expectedValue = "A";
		string* actualValue = testlist.GetFront();

		if (*actualValue == expectedValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 2
}

void Tester::Test_GetBack()
{
    DrawLine();
    cout << "TEST: Test_GetBack" << endl;

    // Put tests here

	{ 	// Test_GetFront - Test 1
		cout << "\nTest 1:\nCreate a list, do nothing.\n"
			<< "GetFront() should return nullptr.\n" << endl;
		List<string> testlist;
		string* actualValue = testlist.GetBack();

		if (actualValue == nullptr) // the expected value is nullptr
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 1


	{ 	// Test_GetFront - Test 2
		cout << "\nTest 2:\nCreate a list, insert \"A\".\n"
			<< "GetFront() should return \"A\".\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		string expectedValue = "A";
		string* actualValue = testlist.GetBack();

		if (*actualValue == expectedValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 2

	{ 	// Test_GetFront - Test 3
		cout << "\nTest 3:\nCreate a list, insert \"A\" at [0] and \"B\" at [1].\n"
			<< "GetFront() should return \"A\".\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		string expectedValue = "B";
		string* actualValue = testlist.GetBack();

		if (*actualValue == expectedValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 3

}

void Tester::Test_GetCountOf()
{
    DrawLine();
    cout << "TEST: Test_GetCountOf" << endl;

    // Put tests here

	{ 	// Test_GetCountOf - Test 1
		cout << "\nTest 1:\nCreate a list, do nothing.\n"
			<< "Input \"A\"\n"
			<< "GetCountOf(\"A\") should return 0.\n" << endl;
		List<string> testlist;
		int expectedValue = 0;
		int actualValue = testlist.GetCountOf("A");

		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (actualValue == expectedValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 1

	{ 	// Test_GetCountOf - Test 2
		cout << "\nTest 2:\nCreate a list, add \"A\",\"B\",\"C\".\n"
			<< "Input \"A\"\n"
			<< "GetCountOf(\"A\") should return 1.\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		int expectedValue = 1;
		int actualValue = testlist.GetCountOf("A");

		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (actualValue == expectedValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 2


	{ 	// Test_GetCountOf - Test 3
		cout << "\nTest 3:\nCreate a list, add \"A\",\"B\",\"C\".\n"
			<< "Input \"D\"\n"
			<< "GetCountOf(\"D\") should return 0.\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		int expectedValue = 0;
		int actualValue = testlist.GetCountOf("D");

		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (actualValue == expectedValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 3

	{ 	// Test_GetCountOf - Test 4
		cout << "\nTest 4:\nCreate a list, add \"A\",\"B\",\"C\", \"A\".\n"
			<< "Input \"A\"\n"
			<< "GetCountOf(\"A\") should return 2.\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		testlist.PushBack("A");
		int expectedValue = 2;
		int actualValue = testlist.GetCountOf("A");

		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual value:   " << actualValue << endl;

		if (actualValue == expectedValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 4
}

void Tester::Test_Contains()
{
    DrawLine();
    cout << "TEST: Test_Contains" << endl;

    // Put tests here

	{ 	// Test_Contains - Test 1
		cout << "\nTest 1:\nCreate a list, do nothing.\n"
			<< "Input \"A\"\n"
			<< "Contains(\"A\") should return false.\n" << endl;
		List<string> testlist;
		bool expectedResult = false;
		bool actualResult = testlist.Contains("A");

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 1

	{ 	// Test_Contains - Test 2
		cout << "\nTest 2:\nCreate a list, add \"A\",\"B\",\"C\".\n"
			<< "Input \"A\"\n"
			<< "Contains(\"A\") should return true.\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		bool expectedResult = true;
		bool actualResult = testlist.Contains("A");

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 2


	{ 	// Test_Contains - Test 3
		cout << "\nTest 3:\nCreate a list, add \"A\",\"B\",\"C\".\n"
			<< "Input \"D\"\n"
			<< "Contains(\"D\") should return false.\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		bool expectedResult = false;
		bool actualResult = testlist.Contains("D");
		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 3

	{ 	// Test_Contains - Test 4
		cout << "\nTest 4:\nCreate a list, add \"A\",\"B\",\"C\",\"A\".\n"
			<< "Input \"A\"\n"
			<< "Contains(\"A\") should return true.\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		testlist.PushBack("A");
		bool expectedResult = true;
		bool actualResult = testlist.Contains("A");

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 4
}

void Tester::Test_RemoveAtIndex()
{
    DrawLine();
    cout << "TEST: Test_RemoveAtIndex" << endl;

    // Put tests here

	{ 	// Test_RemoveAtIndex - Test 1
		cout << "\nTest 1:\nCreate a list, do nothing.\n"
			<< "Input 0.\n"
			<< "Remove(0) should return false.\n" << endl;
		List<string> testlist;
		bool expectedResult = false;
		bool actualResult = testlist.RemoveAtIndex(0);

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 1

	{ 	// Test_RemoveAtIndex - Test 2
		cout << "\nTest 2:\nCreate a list, insert \"A\" at [0].\n"
			<<  "Input 0.\n"
			<< "RemoveAtIndex(0) should return true.\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		bool expectedResult = true;
		bool actualResult = testlist.RemoveAtIndex(0);

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 2

	{ 	// Test_RemoveAtIndex - Test 3
		cout << "\nTest 3:\nCreate a list, insert \"A\" at [0]\n"
			<< "Input 0\n"
			<< "Get(0) should return nullptr.\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		testlist.RemoveAtIndex(0);

		if (testlist.Get(0) == nullptr) // the expected value is nullptr
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 3

}

void Tester::Test_RemoveWithValue() 
{
	DrawLine();
	cout << "TEST: RemoveWithValue" << endl;

	// Put tests here

	{ 	// Test_RemoveWithValue - Test 1
		cout << "\nTest 1:\nCreate a list, do nothing.\n"
			<< "Input \"A\".\n"
			<< "RemoveWithValue(\"A\") should return false.\n" << endl;
		List<string> testlist;
		bool expectedResult = false;
		bool actualResult = testlist.RemoveWithValue("A");

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 1

	{ 	// Test_RemoveWithValue - Test 2
		cout << "\nTest 2:\nCreate a list, insert \"A\" at [0].\n"
			<< "Input \"A\".\n"
			<< "RemoveWithValue(\"A\") should return true.\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		bool expectedResult = true;
		bool actualResult = testlist.RemoveWithValue("A");

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;
		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 2

	{ 	// Test_RemoveAtIndex - Test 3
		cout << "\nTest 3:\nCreate a list, insert \"A\" at [0]\n"
			<< "Input 0\n"
			<< "Get(0) should return nullptr.\n" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		testlist.RemoveWithValue("A");
		string* actualValue = testlist.Get(0);

		if (actualValue == nullptr)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // End test 3
}

void Tester::Test_Insert()
{
    DrawLine();
    cout << "TEST: Test_Insert" << endl;

    // Put tests here

	{ 	// Test_Insert - Test 1
		cout << "\nTest 1:\nCreate a list, do nothing.\n"
			 << "Input 0 and \"A\".\n"
			 << "Insert(int atIndex, const T& item) returns true.\n" << endl;

		List<string> testlist;
		bool expectedResult = true;
		bool actualResult = testlist.Insert(0, "A");

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // Test end

	{ 	// Test_Insert - Test 2
		cout << "\nTest 2:\nCreate a list, insert \"A\" at 0.\n"
			<< "Input 0 and \"B\".\n"
			<< "Get(0) returns \"B\"\n" << endl;

		List<string> testlist;
		testlist.PushBack("A");
		bool expectedResult = true;
		bool actualResult = testlist.Insert(0, "B");
		string expectedValue = "B";
		string* actualValue = testlist.Get(0);

		cout << "Expected value: " << expectedValue << endl;
		cout << "Actual size:   " << *actualValue << endl;

		if (*actualValue == expectedValue)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}


	}   // End test 2

	{ 	// Test_Insert - Test 3
		cout << "\nTest 3:\nCreate a list, insert 100 items.\n"
			<< "Input 0 and \"A\".\n"
			<< "Insert(0, \"A\") returns false.\n" << endl;

		List<string> testlist;

		for (int i = 0; i < 100; i++)
		{
			testlist.PushBack("A");
		}

		bool expectedResult = false;
		bool actualResult = testlist.Insert(0, "A");

		if (expectedResult == 1)
		{
			cout << "Expected result: true" << endl;

		}
		else
		{
			cout << "Expected result: false" << endl;

		}

		if (actualResult == 1)
		{
			cout << "Actual result: true" << endl;
		}
		else
		{
			cout << "Actual result: false" << endl;
		}

		if (actualResult == expectedResult)
		{
			cout << "Test passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}   // Test end

}
