#ifndef _LIST_HPP
#define _LIST_HPP

#include <iostream>
using namespace std;

const int ARRAY_SIZE = 100;

template <typename T>
class List
{
private:
    // private member variables
    int m_itemCount;
	T m_arr[ARRAY_SIZE];

    // functions for interal-workings
    bool ShiftRight( int atIndex )
    {
		
		if (atIndex < 0 || atIndex > m_itemCount)
		{
			return false;
		}

		for (int i = atIndex + 1; i > 0; i--)
		{
			m_arr[i + 1] = m_arr[i];
			m_itemCount++;
		}

		return true;
    }

    bool ShiftLeft( int atIndex )
    {
		if (atIndex < 0 || atIndex > m_itemCount)
		{
			return false;
		}

		for (int i = atIndex + 1; i < m_itemCount; i++)
		{
			m_arr[i - 1] = m_arr[i];
		}
		return true;
    }

public:
    List()
    {
		m_itemCount = 0;
    }

    ~List()
    {
    }

    // Core functionality
    int     Size() const
    {
		return m_itemCount;
    }

    bool    IsEmpty() const
    {
		return (m_itemCount == 0);
    }

    bool    IsFull() const
    {
		return (m_itemCount == ARRAY_SIZE);
    }

    bool    PushFront( const T& newItem )
    {
		if (IsFull())
		{
			return false;
		}

		ShiftRight(0);
		m_arr[0] = newItem;
		return true;
    }

    bool    PushBack( const T& newItem )
    {
		// if the array is full
		// prevent PushBack on a full array
		if (IsFull())
		{
			return false;   
		}

		m_arr[m_itemCount] = newItem;
		m_itemCount++;
		return true; // function was successful
    }

    bool    Insert( int atIndex, const T& item )
    {
		if (IsFull())
		{
			return false;
		}

		if (atIndex == 0)
		{
			return PushFront(item);
		}
		else if (atIndex == m_itemCount)
		{
			return PushBack(item);
		}
    }

    bool    PopFront()
    {
		if (IsEmpty())
		{
			return false;
		}

		// remove item at [0]
		// expect [0] to be overwritten by the item to the right of it
		ShiftLeft(0);
		return true;
    }

    bool    PopBack()
    {
		if (IsEmpty())
		{
			return false;
		}

		m_itemCount--;
		// list is one size smaller
		// will get overwritten with push later
		// lazy deletion
		return true;
    }

    bool    RemoveWithValue( const T& item )
    {
		if (IsEmpty())
		{
			return false;
		}

		// One way you can do this
		int removeAtIndex[ARRAY_SIZE];
		int j = 0;

		// Keep track of indices to remove
		for (int i = 0; i < Size(); i++)
		{
			if (m_arr[i] == item)
			{
				removeAtIndex[j] = i;
				j++;
			}
		}

		// Remove all indices we recorded
		for (int i = 0; i < j; i++)
		{
			RemoveAtIndex(removeAtIndex[i]);
		}
    }

    bool    RemoveAtIndex( int atIndex )
    {
		if (IsEmpty())
		{
			return false;
		}

		if (atIndex == 0)
		{
			PopFront();
			ShiftLeft(0);
			m_itemCount--;
			return true;
		}

		else if (atIndex == m_itemCount - 1)
		{

			PopBack();
			m_itemCount--;
			return true;
		}

		ShiftLeft(atIndex);
		m_itemCount--;
		return true;
    }

    void    Clear()
    {
		// lazy deletion	
		m_itemCount = 0;
    }

    // Accessors
    T*      Get( int atIndex )
    {
		if (atIndex < 0 || atIndex >= m_itemCount)
		{
			return nullptr;
		}

		return &m_arr[atIndex];
    }

    T*      GetFront()
    {
		if (IsEmpty())
		{
			return nullptr;
		}

		return &m_arr[0];
    }

    T*      GetBack()
    {
		if (IsEmpty())
		{
			return nullptr;
		}

		return &m_arr[m_itemCount - 1];
    }

    // Additional functionality
    int     GetCountOf( const T& item ) const
    {
		//set up counter
		int counter = 0;

		for (int i = 0; i < Size(); i++)
		{
			if (m_arr[i] == item)
			{
				counter++;
			}
		}

		return counter;
    }

    bool    Contains( const T& item ) const
    {
		return GetCountOf(item) > 0;

		// if equal to zero, then false
    }

    friend class Tester;
};


#endif
