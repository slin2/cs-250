#include <iostream>
#include <string>
#include "Tester.hpp"

using namespace std;

int main()
{
    Tester tester;
    tester.RunTests();

	cin.ignore();
	cin.get();
    return 0;
}

/*

Output:

--------------------------------------------------------------------------------
TEST: Test_Init

Test 1:
Create a list, do nothing.
Size() should return 0.

Expected size: 0
Actual size:   0
Test passed

Test 2:
Create a list, do nothing.
IsEmpty() should return true.

Expected result: true
Actual result: true
Test passed

Test 3:
Create a list, do nothing.
IsFull() should return false.

Expected result: false
Actual result: false
Test passed

--------------------------------------------------------------------------------
TEST: Test_IsEmpty

Test 1:
Create a list, do nothing.
IsEmpty() returns true.

Expected result: true
Actual result: true
Test passed

Test 2:
Create a list, insert "A"
IsEmpty() returns false.

Expected result: false
Actual result: false
Test passed

--------------------------------------------------------------------------------
TEST: Test_IsFull

Test 1:
Create a list, do nothing.
IsFull() returns false.

Expected result: false
Actual result: false
Test passed

Test 2:
Create a list, insert 2 items.
IsFull() returns false.

Expected result: false
Actual result: false
Test passed

Test 3:
Create a list, insert 100 items.
IsFull() returns true.

Expected result: true
Actual result: true
Test passed

--------------------------------------------------------------------------------
TEST: Test_Size

Test 1:
Create a list, do nothing.
Size() returns 0.

Expected size: 0
Actual size:   0
Test passed

Test 2:
Create a list, insert 1 item.
Size() returns 1.

Expected size: 1
Actual size:   1
Test passed

--------------------------------------------------------------------------------
TEST: Test_GetCountOf

Test 1:
Create a list, do nothing.
Input "A"
GetCountOf("A") should return 0.

Expected value: 0
Actual value:   0
Test passed

Test 2:
Create a list, add "A","B","C".
Input "A"
GetCountOf("A") should return 1.

Expected value: 1
Actual value:   1
Test passed

Test 3:
Create a list, add "A","B","C".
Input "D"
GetCountOf("D") should return 0.

Expected value: 0
Actual value:   0
Test passed

Test 4:
Create a list, add "A","B","C", "A".
Input "A"
GetCountOf("A") should return 2.

Expected value: 2
Actual value:   2
Test passed

--------------------------------------------------------------------------------
TEST: Test_Contains

Test 1:
Create a list, do nothing.
Input "A"
Contains("A") should return false.

Expected result: false
Actual result: false
Test passed

Test 2:
Create a list, add "A","B","C".
Input "A"
Contains("A") should return true.

Expected result: true
Actual result: true
Test passed

Test 3:
Create a list, add "A","B","C".
Input "D"
Contains("D") should return false.

Expected result: false
Actual result: false
Test passed

Test 4:
Create a list, add "A","B","C","A".
Input "A"
Contains("A") should return true.

Expected result: true
Actual result: true
Test passed

--------------------------------------------------------------------------------
TEST: Test_PushFront

Test 1:
Create a list, do nothing.
Input "A"
PushFront("A") should return true. Size() returns 1.

Expected result: true
Actual result: true
Test passed

Expected size: 1
Actual size:   1
Test passed

Test 2:
Create a list, insert 100 items.
Input "A"PushFront("A") should return false.

Expected result: false
Actual result: false
Test passed

Test 3:
Create a list, do nothing.
Input "A", "B", "C"
Get(0) returns "C", Get(1) returns "B", Get(2) returns "A"

Expected value at 0: C
Expected value at 1: B
Expected value at 2: A

Actual value at 0: C
Actual value at 1:
Actual value at 2:
Test Failed

--------------------------------------------------------------------------------
TEST: Test_PushBack

Test 1:
Create a list, do nothing.
Input "A"
PushBack("A") should return true. Size() returns 1.

Expected result: true
Actual result: true
Test passed

Expected size: 1
Actual size:   1
Test passed

Test 2:
Create a list, insert 100 items.
Input "A"
PushBack("A") should return false.

Expected result: false
Actual result: false
Test passed

Test 3:
Create a list, do nothing.
Input "A", "B", "C"
Get(0) returns "A", Get(1) returns "B", Get(3) returns "C"

Expected value at 0: A
Expected value at 1: B
Expected value at 2: C

Actual value at 0: A
Actual value at 1: B
Actual value at 2: C
Test Passed

--------------------------------------------------------------------------------
TEST: Test_Get

Test 1:
Create a list, do nothing.
Input 0
Get(0) should return nullptr.

Test passed

Test 2:
Create a list, insert "A".
Input 0
Get(0) should return "A".

Test passed

--------------------------------------------------------------------------------
TEST: Test_GetFront

Test 1:
Create a list, do nothing.
GetFront() should return nullptr.

Test passed

Test 2:
Create a list, insert "A".
GetFront() should return "A".

Test passed

--------------------------------------------------------------------------------
TEST: Test_GetBack

Test 1:
Create a list, do nothing.
GetFront() should return nullptr.

Test passed

Test 2:
Create a list, insert "A".
GetFront() should return "A".

Test passed

Test 3:
Create a list, insert "A" at [0] and "B" at [1].
GetFront() should return "A".

Test passed

--------------------------------------------------------------------------------
TEST: Test_PopFront

Test 1:
Create a list, do nothing.
PopFront() should return false.

Expected result: false
Actual result: false
Test passed

Test 2:
Create a list, insert "A".
PopFront() should return true.

Expected result: true
Actual result: true
Test passed

Test 3:
Create a List, insert  "A" at [0], "B" at [1].
Get[0] does not equal "A".

Test Passed

--------------------------------------------------------------------------------
TEST: Test_PopBack

Test 1:
Create a list, do nothing.
PopBack() should return false.

Expected result: false
Actual result: false
Test passed

Test 2:
Create a list, insert "A".
PopBack() should return true.

Expected result: true
Actual result: true
Test passed

Test 3:
Create a List, insert  "A" at [0], "B" at [1].
Size() returns 1

Expected size: 1
Actual size:   1
Test passed

--------------------------------------------------------------------------------
TEST: Test_Get

Test 1:
Create a list, do nothing.
Size() should return 0.

Expected size: 0
Actual size:   0
Test passed

Test 1:
Create a list, insert 1 item.
Size() should return 0.

Expected size: 0
Actual size:   0
Test passed

Test 1:
Create a list, insert 100 items.
Size() should return 0.

Expected size: 0
Actual size:   0
Test passed

--------------------------------------------------------------------------------
TEST: Test_ShiftRight

Test 1:
Create a list, do nothing.
Input 0.
ShiftRight(0) returns true.

Expected result: true
Actual result: true
Test passed

Test 2:
Create a List, insert "A" at [0], "B" at [1], "C" at [2].
Input 0.
Get(0) returns "A", Get(2) returns "B", Get(2) returns "C"

Expected result: true
Actual result: true
Test passed

Expected value: A
Actual value:   A
Test passed

Expected value: B
Actual value:   B
Test passed

Expected value: C
Actual value:   C
Test passed

Test 3:
Create a List, insert "A" at [0], "B" at [1], "C" at [2].
Input 3.
ShiftRight(3) returns true.

Expected result: true
Actual result: true
Test passed

--------------------------------------------------------------------------------
TEST: Test_ShiftLeft

Test 1:
Create a list, do nothing.
Input 0.
ShiftLeft(0) returns true.

Expected result: true
Actual result: true
Test passed

Test 2:
Create a List, insert "A" at [0], "B" at [1], "C" at [2].
Input 0.
Get(0) returns "A", Get(1) returns "C"

Expected result: true
Actual result: true
Test passed

Expected value: A
Actual value:   A
Test passed

Expected value: C
Actual value:   C
Test passed

Test 3:
Create a List, insert "A" at [0], "B" at [1], "C" at [2].
Input 3.
ShiftLeft(3) returns true.

Expected result: true
Actual result: true
Test passed

--------------------------------------------------------------------------------
TEST: Test_RemoveAtIndex

Test 1:
Create a list, do nothing.
Input 0.
Remove(0) should return false.

Expected result: false
Actual result: false
Test passed

Test 2:
Create a list, insert "A" at [0].
Input 0.
RemoveAtIndex(0) should return true.

Expected result: true
Actual result: true
Test passed

Test 3:
Create a list, insert "A" at [0]
Input 0
Get(0) should return nullptr.

Test passed

--------------------------------------------------------------------------------
TEST: RemoveWithValue

Test 1:
Create a list, do nothing.
Input "A".
RemoveWithValue("A") should return false.

Expected result: false
Actual result: false
Test passed

Test 2:
Create a list, insert "A" at [0].
Input "A".
RemoveWithValue("A") should return true.

Expected result: true
Actual result: true
Test passed

Test 3:
Create a list, insert "A" at [0]
Input 0
Get(0) should return nullptr.

Test passed

--------------------------------------------------------------------------------
TEST: Test_Insert

Test 1:
Create a list, do nothing.
Input 0 and "A".
Insert(int atIndex, const T& item) returns true.

Expected result: true
Actual result: true
Test passed

Test 2:
Create a list, insert "A" at 0.
Input 0 and "B".
Get(0) returns "B"

Expected value: B
Actual size:   B
Test passed

Test 3:
Create a list, insert 100 items.
Input 0 and "A".
Insert(0, "A") returns false.

Expected result: false
Actual result: false
Test passed



*/


