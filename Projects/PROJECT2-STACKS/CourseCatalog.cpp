#include "CourseCatalog.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

#include "UTILITIES/Menu.hpp"
#include "EXCEPTIONS/CourseNotFoundException.hpp"
#include "DATA_STRUCTURES/Stack.hpp" // LinkedStack

CourseCatalog::CourseCatalog()
{
    LoadCourses();
}

void CourseCatalog::LoadCourses() noexcept // done
{
    Menu::Header( "LOADING COURSES" );

    ifstream input( "courses.txt" );

    if ( !input.is_open() )
    {
        cout << "Error opening input text file, courses.txt" << endl;
        return;
    }

    string label, courseCode, courseName, prerequisite;
    Course newCourse;

    while ( input >> label )
    {
        if ( label == "COURSE" )
        {
            if ( newCourse.name != "" )
            {
                m_courses.PushBack( newCourse );
                newCourse.Clear();
            }

            input >> newCourse.code >> newCourse.name;
        }
        else if ( label == "PREREQ" )
        {
            input >> newCourse.prereq;
        }
    }

    input.close();

    cout << " * " << m_courses.Size() << " courses loaded" << endl << endl;
}

// view all of the courses in the course catalog
void CourseCatalog::ViewCourses() noexcept
{
    Menu::Header( "VIEW COURSES" );

	// create headers
	cout << setw(4) << "#" 
		 << setw(15) << "Course Code" 
		 << setw(46) << "Course Name" 
		 << setw(10) << "Prerequisites" << endl;
	
	// read through the LinkedList and display the information
	// each course should have a code, name, and prerequisite
	for (int i = 0; i < m_courses.Size(); i++)
	{
		cout << setw(4) << i
			 << setw(15) << m_courses[i].code
			 << setw(46) << m_courses[i].name
			 << setw(10) << m_courses[i].prereq 
			 << endl;
	}
}

// for internal use
// find a course based on code input
Course CourseCatalog::FindCourse( const string& code )
{
	for (int i = 0; i < m_courses.Size(); i++)
	{
		if (m_courses[i].code == code)
		{
			return m_courses[i];
		}
	}
	throw CourseNotFound("Error! Unable to find course " + code + "!");
}

// displays the courses a student needs to take if they want to take a certain course
// if there are prerequisites then they will be listed with
// the first course be8hg the first course the student needs to take and the remaining courses
// are the next courses the students need to complete before they can take the target course
// if there are no prerequisites then the student only needs to take the class they wanted to take
void CourseCatalog::ViewPrereqs() noexcept
{
    Menu::Header( "GET PREREQS" );

	// declare variables
	string code;
	Course current;
	LinkedStack<Course> prereqs;

	// prompt for and receive user input
	cout << "Please enter the course code (no spaces)\n" << endl;
	cin >> code;

	try {

		current = FindCourse(code); // find the course based on the input code
	}
	catch (CourseNotFound cnf) {
		cout << cnf.what() << endl; // display error message
		return; // exit the function
	}

	prereqs.Push(current); //add the course to the linked stack

	while (current.prereq != "") // keep looping until there are no prereqs left
	{
		try {

			current = FindCourse(current.prereq); // find the prereq
		}
		catch (CourseNotFound cnf) {
			cout << cnf.what() << endl; // display error message
			break; // break out of the while loop
		}

		prereqs.Push(current); //add  the course to the linked stack
	}

	cout << "\nThe courses you need to take are: \n" << endl;

	while (prereqs.Size() > 0) // while there are prerequisites in the linked stack
	{
			cout << setw(10) << prereqs.Top().code
				<< setw(10) << prereqs.Top().name << endl;

			prereqs.Pop(); // remove the top item
	}
}

void CourseCatalog::Run() noexcept
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU" );

        int choice = Menu::ShowIntMenuWithPrompt( { "View all courses", "Get course prerequisites", "Exit" } );

        switch( choice )
        {
            case 1:
                ViewCourses();
            break;

            case 2:
                ViewPrereqs();
            break;

            case 3:
                done = true;
            break;
        }
    }
}
