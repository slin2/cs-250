#ifndef _STACK_HPP
#define _STACK_HPP

#include "Node.hpp"
#include "LinkedList.hpp"
#include "../EXCEPTIONS/CourseNotFoundException.hpp"

template <typename T>
class LinkedStack
{
    public:
    LinkedStack()
    {
		m_itemCount = 0; // empty stack
    }

	// same as the PushBack() function of the LinkedList.hpp
	// @author Rachel Singh
    void Push( const T& newData )
    {
		// create new node
		Node<T>* newNode = new Node<T>; // node to be added
		newNode->data = newData; // add parameter data to node

		if (m_ptrLast == nullptr) // if the stack is empty
		{
			// point the first/last to the new node
			m_ptrFirst = newNode;
			m_ptrLast = newNode;
		}
		else // list has at least one item
		{
			// point the last node's ptrNext to the new node
			m_ptrLast->ptrNext = newNode; // ptrNext points to the new node
			newNode->ptrPrev = m_ptrLast; // new node's ptrPrev points to the old last node
			m_ptrLast = newNode; // the new node becomes the new last node
		}

		m_itemCount++; // an item was added, so increment
    }

	// same as the GetBack() function of the LinkedList.hpp
	// @author Rachel Singh
	T& Top()
	{
		if (m_ptrLast == nullptr) // if the stack is empty
		{
			throw CourseNotFound("Course not found."); // throw exception
		}
		else // queue not empty
		{
			return m_ptrLast->data; //return the data in the top node
		}
	}

	// same as the PopBack() function of the LinkedList.hpp
	// @author Rachel Singh
    void Pop()
    {
		if (m_ptrFirst == nullptr) // if the stack is empty
		{
			return; // do nothing
		}
		else if (m_ptrFirst == m_ptrLast) // if there is only one item in the stack
		{
			delete m_ptrFirst; // remove the only item
			m_ptrFirst = nullptr;
			m_ptrLast = nullptr;
		}
		else // if there is more than one item in the stack
		{
			Node<T>* ptrSecondToLast = m_ptrLast->ptrPrev; // locate the item that is second from the top
			delete m_ptrLast; // delete the top
			m_ptrLast = ptrSecondToLast; // set the new top
			m_ptrLast->ptrNext = nullptr; // set the ptrNext of the new top to nullpointer
		}

		m_itemCount--; // an item was removed, so decrement
    }

		int Size() {
		return m_itemCount; // the size of the stack is the number of items in the stack
    }

    private:
    Node<T>* m_ptrFirst;
    Node<T>* m_ptrLast;
    int m_itemCount;
};

#endif


