#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{
    public:
    void FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile );
    void RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile );
};

void Processor::FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile )
{
	// set up the output to the logfile
	ofstream output(logFile);
	output << "First Come, First Served" << endl; // header

	int cycles = 0; // variable to keep track of cycles

	// display job id
	output << "Processing job #" << jobQueue.Front()->id << "..." << endl; 

	// while not empty
	while (jobQueue.Size() > 0)
	{
		output << "CYCLE: " << cycles << "\t"; // display cycles

		// process the front-most item
		jobQueue.Front()->Work(FCFS);

		output << "REMAINING: " << jobQueue.Front()->fcfs_timeRemaining << endl; // display the remaining time
		// if the front-most item is done
		
		// if the front job is finished
		if (jobQueue.Front()->fcfs_done)
		{
			// set the front-most item's finish time
			jobQueue.Front()->SetFinishTime(cycles, FCFS);
			// pop the item off the jobqueue
			jobQueue.Pop();
			
			// make sure the queue is not empty and display the job id again
			if (jobQueue.Size() > 0)
			{
				output << "Done!" << endl;
				output << "***************************************" << endl;
				output << "Processing job #" << jobQueue.Front()->id << "..." << endl;
			}
		}
		
		cycles++; // increment the cycles variable

	}

	// write a summary to the output file

	output << "***************************************" << endl;

	int sum = 0; 
	double avg = 0.0;

	for (unsigned int i = 0; i < allJobs.size(); i++)
	{
		sum += allJobs[i].fcfs_finishTime; // sum of all finished time
	}

	avg = (double)sum / allJobs.size(); // calculate the average
	

	output << "First come, first serve results: \n" << endl;

	for (unsigned int i = 0; i < allJobs.size(); i++)
	{
		output << "JOB ID: " << i << "\t";
		output << "TIME TO COMPLETE: " << allJobs[i].fcfs_finishTime << "\n" << endl;
	}

		  
    output << "Total Time :" << sum << "\t"
		   << "Average Time: " << avg << endl;

	output.close(); // close the stream
}

void Processor::RoundRobin(vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile)
{

	// set up the output to the logfile
	ofstream output(logFile);
	output << "Round Robin" << endl; // header

	int cycles = 0; // variable to keep track of the number of cycles
	int timer = 0; // variable to keep track of the time

	output << "Processing job #" << jobQueue.Front()->id << "..." << endl; // display the job id

	// while jobqueue not empty
	while (jobQueue.Size() > 0)
	{
		output << "CYCLE: " << cycles << "\t"; // display which cycle the program is on

		// if the timer has hit the timePerProcess value
		if (timer == timePerProcess)
		{
			// increment the front-most job's r_timesInterrupted by 1
			jobQueue.Front()->rr_timesInterrupted += 1;
			// push front job to back
			jobQueue.Push(jobQueue.Front());
			// pop the job off the front of the queue
			jobQueue.Pop();
			// reset timer to zero
			timer = 0;
			
			// display the next job id
			output << "Done\n" << endl;
			output << "**************************************************\n" << endl;
			output << "Processing job #" << jobQueue.Front()->id << "..." << endl;

		}

		//process front item
		jobQueue.Front()->Work(RR);

		output << "REMAINING: " << jobQueue.Front()->rr_timeRemaining << endl; // display the time remaining

		//if the front most item is done 
		if (jobQueue.Front()->rr_done)
		{
			// set the front-most item's finish time
			jobQueue.Front()->SetFinishTime(cycles, RR);
			// pop the item off the jobqueue
			jobQueue.Pop();
		}

		// increment the cycle counter and time counter
		cycles++;
		timer++;
	}

	// write a summary to the output file
	
	int sum = 0;
	double avg = 0.0;

	for (unsigned int i = 0; i < allJobs.size(); i++)
	{
		sum += allJobs[i].rr_finishTime; // calculate the sum of all finishTimes
	}

	avg = (double)sum / allJobs.size(); // calculate the average

	output << "**************************************************\n" << endl;
	output << "Round Robin results: \n" << endl;

	for (unsigned int i = 0; i < allJobs.size(); i++)
	{
		output << "JOB ID: " << i << "\t";
		output << "TIME TO COMPLETE: " << allJobs[i].rr_finishTime << "\t";
		output << "TIMES INTERRUPTED: " << allJobs[i].rr_timesInterrupted << "\n" << endl;
	}


	output << "Total Time :" << sum << "\n"
		<< "Average Time: " << avg << "\n"
		<< "Round Robin Interval: " << timePerProcess << "\n"
		<< endl;

	output.close(); // close the stream
}

#endif
