#include <iostream>
#include <fstream>
#include <list>
#include <string>
using namespace std;

#include "Menu.hpp"

list<string> LoadBook( const string& filename );
void ReadBook( list<string> bookText );

int main()
{
    vector<string> books = { "aesop.txt", "fairytales.txt" };

    bool done = false;
    while ( !done )
    {
        Menu::Header( "LIBRARY" );

        cout << "Which book do you want to read?" << endl;
        int choice = Menu::ShowIntMenuWithPrompt( books );

        list<string> bookText = LoadBook( books[choice-1] );
        ReadBook( bookText );
    }

    return 0;
}


list<string> LoadBook( const string& filename )
{
    list<string> bookText;

    cout << "Loading " << filename << "..." << endl;

    ifstream input( filename );

    if ( !input.good() )
    {
        cout << "Error opening file" << endl;
    }

    string line;
    while ( getline( input, line ) )
    {
        bookText.push_back( line );
    }

    cout << endl << bookText.size() << " lines loaded" << endl << endl;

    input.close();

    return bookText;
}


void ReadBook( list<string> bookText )
{
	// declare and initialize variables
	int counter = 0; // to decide when to end the page
	int lines = 0; // the line count for the current progress of the book
	int pageLength = 28; // amount of lines to draw before ending
	
	// create iterator
	list<string>::iterator it;

	for (it = bookText.begin(); it != bookText.end(); )
	{
		cout << *it << endl; // print the contents
		counter++; 
		lines++;
		it++;

		if (counter == pageLength) // filled pages
		{
			cout << "Line " << lines << " of " << bookText.size() << endl << endl; // display line number

			Menu::DrawHorizontalBar(80); // add a horizontal bar
			int choice = Menu::ShowIntMenuWithPrompt({ "BACKWARD", "FORWARD" }, false); // display options

			if (choice == 1) // backwards
			{
				if (it != bookText.begin()) // not at beginning
				{
					for (int i = 0; i < pageLength; i++)
					{
						lines--;
					}

					lines -= pageLength * 2; // move backwards
				}
			}
			counter = 0; // reset line/page counter
		}
	}
}

