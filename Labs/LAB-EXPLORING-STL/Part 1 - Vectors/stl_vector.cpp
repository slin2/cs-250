// Lab - Standard Template Library - Part 1 - Vectors
// Sen Lin

#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main()
{
	vector<string> courses; // string array of courses

	bool done = false;
	int choice;
	string courseName;

	while (!done)
	{
		cout << "\n1. Add a new course\n"
			<< "2. Remove the last course\n"
			<< "3. Display the course list\n"
			<< "4. Quit\n" << endl;

		cin >> choice;

		if (choice == 1)
		{
			cout << "Please enter the course: " << endl;
			//cin >> courseName;
			cin.ignore(); // needed for getline
			getline(cin, courseName); // allow spaces
			courses.push_back(courseName); // add to list of courses
		}
		else if (choice == 2)
		{
			courses.pop_back(); // remove last item in list of courses
			cout << "The last course is removed\n" << endl;
		}
		else if (choice == 3)
		{
			cout << "The courses in the course list are: \n" << endl;

			for (unsigned int i = 0; i < courses.size(); i++)
			{
				cout << i <<". " << courses[i] << endl;
			}
		}
		else if (choice == 4) {
			done = true;
		}

	}

    cin.ignore();
    cin.get();
    return 0;
}


/*

Output 

1. Add a new course
2. Remove the last course
3. Display the course list
4. Quit

1
Please enter the course:
CIS 230

1. Add a new course
2. Remove the last course
3. Display the course list
4. Quit

1
Please enter the course:
CIS 245

1. Add a new course
2. Remove the last course
3. Display the course list
4. Quit

2
The last course is removed


1. Add a new course
2. Remove the last course
3. Display the course list
4. Quit

3
The courses in the course list are:

0. CIS 230

1. Add a new course
2. Remove the last course
3. Display the course list
4. Quit

4


*/